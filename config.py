#!/usr/bin/env python3
# -*- coding:  utf-8 -*-
__author__ = 'Rizha'


def holiday_event():
    return {
        "2014": {
            "12": {
                "25": "Hari Raya Natal"
            }
        },
        "2015": {
            "01": {
                "01": "Tahun Baru Masehi",
                "03": "Maulid Nabi Muhammad SAW"
            },
            "02": {
                "19": "Tahun Baru Imlek 2556"
            },
            "03": {
                "21": "Hari Raya Nyepi Tahun Baru Saka 1937"
            },
            "04": {
                "03": "Wafat Yesus Kristus"
            },
            "05": {
                "01": "Hari Buruh International",
                "14": "Kenaikan Yesus Kristus",
                "16": "Isra Miraj"
            },
            "06": {
                "02": "Hari Raya Waisak 2559"
            },
            "07": {
                "17": "Idul Fitri 1436 Hijriyah",
                "18": "Idul Fitri 1436 Hijriyah"
            },
            "08": {
                "17": "Hari Kemerdekaan RI"
            },
            "09": {
                "24": "Idul Adha 1436 Hijriyah"
            },
            "10": {
                "14": "Tahun Baru Islam 1437 Hijriyah"
            },
            "12": {
                "25": "Hari Raya Natal"
            }
        }
    }

"""
Connection
DATABASE
public 10.10.252.181:
private 10.10.202.15

REDIS
public 10.10.252.130:6379
private 10.10.202.16:6379
production: p01rds01.qiudfc.0001.apse1.cache.amazonaws.com

MONGODB
public 10.10.251.150
private 10.10.121.10
"""
connection =  {"db": "10.10.202.15", "redis": "p01rds01.qiudfc.0001.apse1.cache.amazonaws.com",
               "mongodb": '10.10.121.10'}
