#!/usr/bin/env python3
# -*- coding:  utf-8 -*-
__author__ = 'Gembel Sakti'
__version__ = "Partai Pengemis"
__email__ = "pseudecoder@gmail.com"

import logging
import json
import datetime
import time
import re

import requests
import pymysql
import redis
import pymongo

from config import holiday_event, connection


class HomePageCron(object):

    def __init__(self):

        # Logging config
        logging.basicConfig(format='%(asctime)s %(message)s',
                            datefmt='%m/%d/%Y %I:%M:%S',
                            filename='homepage.log',
                            level=logging.INFO)

        # base url of api
        self.base_url = "http://api_dev.pegipegi.com/"

        # mysql connection
        self.db = pymysql.connect(connection['db'], 'flight', 'X5E8mE0S',
                                  'flightdb')

        # redis connection
        pool = redis.ConnectionPool(host=connection['redis'], port=6379,
                                    db=0)
        self.redis_cache = redis.Redis(connection_pool=pool)

        # mongodb connection
        client = pymongo.MongoClient(connection['mongodb'], 27017)
        db = client.flightdb
        self.collection = db.flightDetail

        # base search url
        self.search_url = "index.php/flight/v1/search/airline/oneway/" \
                          "%s/%s/%s/1/0/0"
        self.search_seq_url = "index.php/flight/v1/search/airline-sequence/"
        self.search_url = self.base_url + self.search_url
        self.search_seq_url = self.base_url + self.search_seq_url

    @staticmethod
    def best_price_promo(response):
        price = None
        promo = None
        for data in response['data'].values():
            if isinstance(data['adultFare'], str):
                data['adultFare'] = re.sub(',', '.', data['adultFare'])    

            if price is None:
                price = data['adultFare']
                promo = data['promo']
                
            if float(data['adultFare']) < float(price):
                price = data['adultFare']
                promo = data['promo']

        return {"price": price, "promo": promo}

    @staticmethod
    def airline_search(url):
        resp = requests.get(url)
        return resp.json()

    @staticmethod
    def airline_search_sequence(url):
        resp = requests.get(url)
        return resp.json()

    def exec_airline_search(self, url):
        resp_search = self.airline_search(url)

        if resp_search['isFinish'] is True:
            pass
        else:
            search_seq_url = self.search_seq_url + resp_search['seq_id']
            i = 1
            while True:
                resp_search_seq = self.airline_search_sequence(search_seq_url)
                if resp_search_seq['isFinish'] is True or i is 60:
                    resp_search = self.airline_search(url)
                    break
                if i % 5 is 0:
                    resp_search = self.airline_search(url)
                    if resp_search['isFinish'] is True:
                        break
                time.sleep(1)
                i += 1

        return resp_search

    def save_cache(self, key, value):
        self.redis_cache.set(key, value)


class Calendar(HomePageCron):
    """Calendar cron will save the result of it to redis cache"""

    def __init__(self, date_start, date_end):

        HomePageCron.__init__(self)
        self.date_start = date_start
        self.date_end = date_end

    def runall_date(self, flight_from, flight_to):
        start = datetime.datetime.strptime(self.date_start, "%Y-%m-%d")
        end = datetime.datetime.strptime(self.date_end, "%Y-%m-%d")
        step = datetime.timedelta(days=1)

        #we start from tomorrow not today
        start += step
        while start <= end:
            date = start.date().isoformat()
            split_date = date.split("-")
            date_day = split_date[2]

            logging.info("Calendar from %s TO %s is runnning in %s",
                            flight_from, flight_to, date)

            date_month = split_date[1]
            date_year = split_date[0]
            keyid = "%s_%s_%s" % (flight_from, flight_to, ''.join(split_date))
            searchkey = {"_id": keyid}
            resp = self.collection.find_one(searchkey)

            #url = self.search_url % (flight_from, flight_to, date)
            #resp = self.exec_airline_search(url)

            price = None
            holiday = False
            promo = False

            try:
                if holiday_event()[date_year][date_month][date_day]:
                    holiday = True
            except KeyError:
                pass

            if resp is None:
                pass
            elif resp['data']:
                # insert_data
                best_price_promo = self.best_price_promo(resp)
                price = best_price_promo['price']
                promo = best_price_promo['promo']

            if price==False:
                price = None

            data = {"date": date, "price": price, "promo": bool(promo),
                    "holiday": holiday, "flight_from": flight_from,
                    "flight_to": flight_to}
            value = json.dumps(data)
            cache_key = format("calendar_%s_%s_%s" % (flight_from,
                                                      flight_to, date))
            self.save_cache(cache_key, value)
            start += step

    def runall_bestroute(self):
        for fligth_route in self.best_routes():
            logging.info("Starting Calendar jobs from %s to %s with starting"
                            " date %s until %s", fligth_route[1],
                            fligth_route[2], self.date_start, self.date_end)

            self.runall_date(fligth_route[1], fligth_route[2])

            logging.info("Calendar jobs route from %s to %s with starting "
                            "date %s until %s is done!", fligth_route[1],
                            fligth_route[2], self.date_start, self.date_end)

    def run(self):
        self.runall_bestroute()
        logging.info("calender job is done!")

    def best_routes(self):
        """
        Get best route from database
        :params :
        :return: tupple best route
        """
        cursor = self.db.cursor()
        query = 'select * from best_routes;'
        cursor.execute(query)
        return cursor.fetchall()

    def payload(self, flight_from, flight_to):
        """Prepare data send to api
        :param flight_from: string date ex: yyyy-mm-dd
        :param flight_to: string date ex: yyyy-mm-dd
        :return: dictionary
        """
        payload = {"device": "android", "flight_from": flight_from,
                   "flight_to": flight_to, "date_start": self.date_start,
                   "date_end": self.date_end, "cron": True}
        return payload


class BestPrice(HomePageCron):
    """
    Best price for home page with matrix route of
    cgk, sub, dps, kno, srg, bdo
    """
    def __init__(self):
        HomePageCron.__init__(self)
        self.best_price = {}
        self.bestprice_date = ""

    def run(self, date):
        logging.info("Starting jobs for best price matix for %s Date!", date)
        data = self.best_price_route(date=date)
        self.save_cache('best_price_' + self.bestprice_date, json.dumps(data))
        logging.info("Best Price Matrix Jobs for %s Date is Done!", date)

    def update_bestprice(self, flight_from, flight_to, resp):
        flight_price = int(self.best_price_promo(resp)['price'])
        if flight_from not in self.best_price:
            self.best_price[flight_from] = {flight_to: flight_price}
        else:
            self.best_price[flight_from].update({flight_to: flight_price})

    def best_price_route(self, date=None):
        flight_route = self.flight_route(date)
        for flight_from, flight_to, flight_date in flight_route:
            logging.info("Now Best Price Matrix Jobs From %s to %s "
                            "is Running", flight_from, flight_to)
            # url = format(self.search_url % (flight_from, flight_to,
            #                                 flight_date))
            # resp = self.exec_airline_search(url)['_embeded']
            
            flight_date = re.sub("\D", '', flight_date)
            keyid = "%s_%s_%s" % (flight_from, flight_to, flight_date)
            resp = self.collection.find_one({"_id": keyid})
            if not resp:
                continue
            # resp = {'data': resp}
            self.update_bestprice(flight_from, flight_to, resp)

        return self.best_price

    def flight_route(self, date):
        """
        Set list flight from, flight to, and flight date
        :return:
        """
        flight = []

        for i in range(6):
            list_route = ['CGK', 'SUB', 'DPS', 'KNO', 'SRG', 'JOG']
            flight_from = list_route.pop(i)
            flight.extend([(flight_from, j, date) for j in list_route])

        self.bestprice_date = date
        return flight


if __name__ == '__main__':
    date_today = datetime.date.today()

    # # Calendar
    six_months = (date_today + datetime.timedelta(6*365/12)).isoformat()
    cron_calendar = Calendar(date_today.isoformat(), six_months)
    cron_calendar.run()

    # Best price matrix
    cron_bestprice = BestPrice()
    for day in range(2):
        requestdate = (date_today + datetime.timedelta(days=day+1))\
            .isoformat()
        cron_bestprice.run(requestdate)
